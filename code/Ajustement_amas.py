import numpy as np
import scipy as sp
import scipy.optimize as opt
import matplotlib.pyplot as plt
plt.ion()  #Pour que matplotlib affiche le plot direct

from astropy.io import fits

#Le nombre de cluster utilise pour le calcul
N_cluster = 300
#Nombre de longueurs d'onde utilisees, a priori 6
wavelength=[100,143,217,353,545,857]
N_wavelength = len(wavelength)

N_max_pix_fit = 39
centre_carte = 100
centre_carte_fit = N_max_pix_fit/2.+0.5


#============================================================
#Ouverture du FITS
#============================================================
multi_wavelength_minimap = fits.open('/home/averliat/P3_gboileau_averliat/Minimap/Multiwavelength_minimap_Ncluster='+str(N_cluster)+'.fits')
multi_wavelength_minimap = multi_wavelength_minimap[0].data



#============================================================
#Definition de la fonction gaussienne 2D pour ajustement
#============================================================


def twoD_Gaussian(x_tuple, amplitude, sigma_x, sigma_y, theta, offset):
        (x,y) = x_tuple
        xo = float(centre_carte_fit)  #Fixe par la position des amas
        yo = float(centre_carte_fit)    
        a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
        b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
        c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
        g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo) 
                            + c*((y-yo)**2)))
        return g.ravel()  #Doit sortir un tableau 1D



#============================================================
#Paramètres d'ajustement initiaux approximatifs
#============================================================
p_0_100 = [1e-2,10,10,0,0.1]
p_0_143 = [3e-3,10,10,0,0.02]
p_0_217 = [0.1,10,10,0,0.1]
p_0_353 = [0.5,10,10,0,0.38]
p_0_545 = [1.4,10,10,0,1.2]
p_0_857 = [3.6,10,10,0,3.1]
p_0 = [p_0_100,p_0_143,p_0_217,p_0_353,p_0_545,p_0_857]



#============================================================
#Ajustement, trace, residus
#============================================================
index_wavelength=0

POPT,PCOV = [],[]
DATA_FIT = []
fig, axes = plt.subplots(ncols=N_wavelength)
fig.suptitle("Residu - Empilement des imagettes de " + str(N_cluster) + " amas du catalogue et de l'ajustement gaussien 2D")


for minimap, ax in zip(multi_wavelength_minimap, axes):
	x_pour_fit = np.linspace(1, N_max_pix_fit, N_max_pix_fit)  #Definition necessaire pour fit
	y_pour_fit = np.linspace(1, N_max_pix_fit, N_max_pix_fit)
	x_gauss, y_gauss = np.meshgrid(x_pour_fit, y_pour_fit)
	x_gauss_tuple = (x_gauss, y_gauss)
	minimap_data = minimap[int(centre_carte-N_max_pix_fit/2.+0.5)-1:int(centre_carte+N_max_pix_fit/2.-0.5),int(centre_carte-N_max_pix_fit/2.+0.5)-1:int(centre_carte+N_max_pix_fit/2.-0.5)].ravel()  #Doit etre un tableau 1D pour fit
	
	popt, pcov = opt.curve_fit(twoD_Gaussian, x_gauss_tuple, minimap_data, p0=p_0[index_wavelength])  #Fit

	data_fitted = twoD_Gaussian(x_gauss_tuple, *popt)  #Gaussienne avec parametre du fit
	
	residu_wavelength = np.array(minimap_data.reshape(N_max_pix_fit,N_max_pix_fit)) - np.array(data_fitted.reshape(N_max_pix_fit,N_max_pix_fit))

	POPT.append(popt)  #Stockage des parametre optimaux de cette frequence
	PCOV.append(pcov)
	ax.hold(True)

	#ax.imshow(minimap_data.reshape(N_max_pix_fit,N_max_pix_fit), origin='bottom')
	ax.set_title(np.str(wavelength[index_wavelength])+"GHz")
	#ax.contour(x_gauss, y_gauss, data_fitted.reshape(N_max_pix_fit,N_max_pix_fit), 4, colors='w')
	ax.imshow(residu_wavelength, origin='bottom')  #Decommenter et commenter le plot de minimap 3 lignes plus haut pour tracer les residus
	index_wavelength += 1
	


plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
fig.colorbar(axes[0].get_images()[0], orientation='horizontal',ax=axes.ravel().tolist())



