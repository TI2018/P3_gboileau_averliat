import numpy as np
import scipy as sp

import matplotlib.pyplot as plt
plt.ion()  #Pour que matplotlib affiche le plot direct

from astropy.io import fits
from astropy.table import Table

import healpy as hp

from astropy.io import fits




#catalogue = fits.open('/home/abeelen/Planck/PSZ/PSZ2v1.fits.gz', memmap=True)
#catalogue.info()
#catalogue['PSZ2_UNION'].data
#Table(catalogue['PSZ2_UNION'].data)

#Table.read('/home/abeelen/Planck/PSZ/PSZ2v1.fits.gz')
catalogue = Table.read('/home/abeelen/Planck/PSZ/PSZ2v1.fits.gz')
#cat.keys()
#cat['SNR']
catalogue.sort('SNR')
catalogue.reverse()
#cat['SNR']
#cat[0:2]
#cluster = cat[0]
#cluster['SNR']
#cluster['RA']

#print(catalogue['RA','DEC'][0:2])

ra_cluster_test = catalogue['RA'][0]
#print(ra_cluster_test)

dec_cluster_test = catalogue['DEC'][0]
#print(dec_cluster_test)






map_planck_1 = hp.read_map('/home/averliat/Map_planck/HFI_SkyMap_100_2048_R2.00_full.fits')

#hp.mollview(test_map, coord=['G'], title= 'testplot', unit='mK', norm ='hist', min = -0.5, max=0.5, xsize=2000)

