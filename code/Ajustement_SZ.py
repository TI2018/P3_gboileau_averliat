# -*- coding: utf-8 -*-
import numpy as np
import scipy.optimize as spopt
import matplotlib.pyplot as plt
from random import randint

x=np.array([i for i in range(100)]) #un beau vecteur
bruit = []
for i in range(100):
	i=i+1
	bruit.append(randint(0,1)*1) #du bruit généré manuellement
y= 10*np.exp(-0.01*(x-50)**(2)) +0.5*x + 1 + bruit #des données bruitées
#y = np.cos(0.1*x)+2 + bruit
y_err=2*np.ones(len(x))
x_err=2*np.ones(len(x)) #les barres d'erreurs

#on définit la fonction par laquelle on va vouloir fitter ET sa dérivée par rapport à x (il faut faire le calcul soi-même)
def func(mu,p):
    y, T_CMB=p
    
    x = mu*h/k_B/T_CMB
    I_0 = 2*k_B*(T_CMB)**3 / (h*c) / (h*c)
    g = x**4*np.exp(x)/(np.exp(x) - 1)/ (np.exp(x) - 1) * (x * (np.exp(x) + 1) / (np.exp(x) - 1) - 4)
    return  I_0*y*g
    #a,b=p
    #return np.cos(a*x) + b

def funcprim(mu,p):
   y, T_CMB=p
    
    x = mu*h/k_B/T_CMB
    I_0 = 2*k_B*(T_CMB)**3 / (h*c) / (h*c)
    g_prim = -x**3*np.exp(x)*((x**2 -9*x + 16)*np.exp(2*x) + 4*(x**2 - 8)*np.exp(x) + x**2 + 9*x + 16) / ((np.exp(x) - 1)**4)
    return I_0*y*g_prim
    #a,b=p
    #return -a*np.sin(a*x)

#on doit donner des valeurs initiales aux paramètres du fit
param_0=np.array([3,10**7])



#normalement vous n'avez pas besoin de modifier le code en dessous de cette ligne

#on définit la fonction "résidus"
def residuals(p, y, x):
    err = (y-func(x,p))/np.sqrt(y_err**2 + (funcprim(x,p)*x_err)**2)
    return err

#on fait un fit moindre carrés en minimisant le carré de la fonction "résidus"
result=spopt.leastsq(residuals,param_0,args=(y, x),Dfun=None, full_output=1)
print('Modélisation avec erreurs sur X et Y')
print('Paramètres optimaux')
print(result[0])
print('Erreurs sur les paramètres')
print(np.sqrt(np.abs(np.diagonal(result[1]))))
#si on considère que les erreurs sont gaussiennes, ces valeurs donnent l'intervalle de confiance à 68%, si on veut un intervalle de confiance à 95% il faut mutliplier ces valeurs par 2

popt=np.copy(result[0]) #on récupère un vecteur avec les paramètres optimaux
#on regarde ce que ça donne sur une courbe
plt.figure
plt.errorbar(x,y,xerr=x_err,yerr=y_err,fmt='b+')
plt.plot(x,func(x,popt),'r')
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Fit avec erreurs sur X et Y')
plt.show()
