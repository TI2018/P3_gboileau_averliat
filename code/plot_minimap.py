import numpy as np
import scipy as sp

import matplotlib.pyplot as plt
plt.ion()  #Pour que matplotlib affiche le plot direct

from astropy.io import fits

#Le nombre de cluster utilise pour le calcul
N_cluster = 300
#Nombre de longueurs d'onde utilisees, a priori 6
wavelength=[100,143,217,353,545,857]
N_wavelength = len(wavelength)


#============================================================
#Ouverture du FITS
#============================================================
multi_wavelength_minimap = fits.open('/home/averliat/P3_gboileau_averliat/Minimap/Multiwavelength_minimap_Ncluster='+str(N_cluster)+'.fits')
multi_wavelength_minimap = multi_wavelength_minimap[0].data



#============================================================
#Plot de la minimap stackee
#============================================================


fig, axes = plt.subplots(ncols=N_wavelength)
nbr_subplot = 100+N_wavelength*10

fig.suptitle("Empilement des imagettes de " + str(N_cluster) + " amas du catalogue")
index = 0
for minimap, ax in zip(multi_wavelength_minimap, axes):
	ax.imshow(minimap, origin = 'lower')
	ax.set_title(np.str(wavelength[index])+"GHz")
	index+=1

plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
fig.colorbar(axes[0].get_images()[0], orientation='horizontal',ax=axes.ravel().tolist())

plt.show()

"""
fig, axes = plt.subplots(N_wavelength, 1)



plt.title("Empilement des imagettes de " + str(N_cluster) + " amas du catalogue")


for l in range(N_wavelength):
#extent=[longitude_top_left,longitude_top_right,latitude_bottom_left,latitude_top_left]

	axes[l,0].imshow(multi_wavelength_minimap[l], origin = 'lower')#, extent=extent)

plt.colorbar()

plt.show()
"""

print("")
