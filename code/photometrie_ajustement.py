import numpy as np
import scipy as sp
import scipy.optimize as opt
import matplotlib.pyplot as plt
plt.ion()  #Pour que matplotlib affiche le plot direct

from astropy.io import fits

#Le nombre de cluster utilise pour le calcul
N_cluster = 300
#Nombre de longueurs d'onde utilisees, a priori 6
wavelength=[100,143,217,353,545,857]
N_wavelength = len(wavelength)

FWHM_beam = [9.66, 7.27, 5.01, 4.86, 4.84, 4.63]

N_max_pix_fit = 199
centre_carte = 100
centre_carte_fit = N_max_pix_fit/2.+0.5

R_max = 20  #Pour photometrie en unite de FWHM
R_min = 15
R_flux = 3


#============================================================
#Ouverture du FITS
#============================================================
multi_wavelength_minimap = fits.open('/home/averliat/P3_gboileau_averliat/Minimap/Multiwavelength_minimap_Ncluster='+str(N_cluster)+'.fits')
multi_wavelength_minimap = multi_wavelength_minimap[0].data



#============================================================
#Fonction cercle pour verification photometrie
#============================================================
def Circle(x,y):
	return ( np.sqrt((x-centre_carte_fit)**2 + (y-centre_carte_fit)**2) )



#============================================================
#Definition de la fonction gaussienne 2D pour ajustement
#============================================================
def twoD_Gaussian(x_tuple, amplitude, sigma_x, sigma_y, theta, offset):
        (x,y) = x_tuple
        xo = float(centre_carte_fit)  #Fixe par la position des amas
        yo = float(centre_carte_fit)    
        a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
        b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
        c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
        g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo) 
                            + c*((y-yo)**2)))
        return g.ravel()  #Doit sortir un tableau 1D



#============================================================
#Fonction determination si pixel dans un cercle de rayon R
#============================================================
def test_cercle(x,y,R):
	if x**2 + y**2 > R**2:
		return False
	else:
		return True



#============================================================
#Photometrie
#============================================================
def calcul_flux(Liste_x,Liste_y,R,centre,TAB):
	flux = 0
	N_pix_flux = 0
	for x in range(len(Liste_x)):
		for y in range(len(Liste_y)):
			if test_cercle(x-centre,y-centre,R) == True:
				flux += TAB[x,y]
				N_pix_flux += 1
				#print (N_pix_flux, flux)

	return flux, N_pix_flux



#============================================================
#Paramètres d'ajustement initiaux approximatifs
#============================================================
p_0_100 = [1e-2,10,10,0,0.1]
p_0_143 = [3e-3,10,10,0,0.02]
p_0_217 = [0.1,10,10,0,0.1]
p_0_353 = [0.5,10,10,0,0.38]
p_0_545 = [1.4,10,10,0,1.2]
p_0_857 = [3.6,10,10,0,3.1]
p_0 = [p_0_100,p_0_143,p_0_217,p_0_353,p_0_545,p_0_857]



#============================================================
#Ajustement, trace, residus, photometrie
#============================================================
index_wavelength=0

POPT,PCOV = [],[]
DATA_FIT = []
fig, axes = plt.subplots(ncols=N_wavelength)
fig.suptitle("Empilement des imagettes de " + str(N_cluster) + " amas du catalogue et contour 'moyen' du fit gaussien")

list_flux = []  #Liste pour stocker les flux de chaque longueurs d'onde

for minimap, ax in zip(multi_wavelength_minimap, axes):
	x_pour_fit = np.linspace(1, N_max_pix_fit, N_max_pix_fit)  #Definition necessaire pour fit
	y_pour_fit = np.linspace(1, N_max_pix_fit, N_max_pix_fit)
	x_gauss, y_gauss = np.meshgrid(x_pour_fit, y_pour_fit)
	x_gauss_tuple = (x_gauss, y_gauss)
	minimap_data = minimap[int(centre_carte-N_max_pix_fit/2.+0.5)-1:int(centre_carte+N_max_pix_fit/2.-0.5),int(centre_carte-N_max_pix_fit/2.+0.5)-1:int(centre_carte+N_max_pix_fit/2.-0.5)].ravel()  #Doit etre un tableau 1D pour fit
	
	popt, pcov = opt.curve_fit(twoD_Gaussian, x_gauss_tuple, minimap_data, p0=p_0[index_wavelength])  #Fit

	data_fitted = twoD_Gaussian(x_gauss_tuple, *popt)  #Gaussienne avec parametre du fit
	
	residu_wavelength = np.array(minimap_data.reshape(N_max_pix_fit,N_max_pix_fit)) - np.array(data_fitted.reshape(N_max_pix_fit,N_max_pix_fit))

	POPT.append(popt)  #Stockage des parametre optimaux de cette frequence
	PCOV.append(pcov)

	#sigma_gauss = np.sqrt( popt[1]**2 + popt[2]**2 )
	#FWHM = 2.*np.sqrt(2*np.log(2))*sigma_gauss

	FWHM = FWHM_beam[index_wavelength]

	#Photometrie
	
	flux, N_pix_flux = calcul_flux(x_pour_fit, y_pour_fit, R_flux*FWHM/2., centre_carte_fit, minimap_data.reshape(N_max_pix_fit,N_max_pix_fit))

	coquille_R_max, N_pix_Rmax = calcul_flux(x_pour_fit, y_pour_fit, R_max*FWHM/2., centre_carte_fit, minimap_data.reshape(N_max_pix_fit,N_max_pix_fit)) 
	
	coquille_R_min, N_pix_Rmin = calcul_flux(x_pour_fit, y_pour_fit, R_min*FWHM/2., centre_carte_fit, minimap_data.reshape(N_max_pix_fit,N_max_pix_fit)) 

	N_pix_fond_ciel = N_pix_Rmax - N_pix_Rmin
	fond_ciel_par_pix = (coquille_R_max - coquille_R_min) / N_pix_fond_ciel

	flux = flux - N_pix_flux*fond_ciel_par_pix  #Unite : MJY/str
	
	list_flux.append(flux)

	
	mediane_minimap = np.median(minimap_data.reshape(N_max_pix_fit,N_max_pix_fit))
	minimap_normalisee = minimap_data.reshape(N_max_pix_fit,N_max_pix_fit) - mediane_minimap


	#Trace
	ax.hold(True)
	ax.imshow(minimap_data.reshape(N_max_pix_fit,N_max_pix_fit), origin='bottom')#, vmin=-5e-2, vmax=4)
	#ax.imshow(minimap_normalisee, origin='bottom', vmin=-0.03, vmax=0.014)
	#ax.imshow(data_fitted.reshape(N_max_pix_fit,N_max_pix_fit), origin='bottom') #Plot des gaussiennes fittees
	ax.set_title(np.str(wavelength[index_wavelength])+"GHz")

	#Contour
	#name_contour = [FWHM, 2*FWHM, 3*FWHM, 30*FWHM, 50*FWHM]
	#color = ['w', 'r', 'b', 'purple', 'orange']
	name_contour = [R_flux*FWHM/2., R_min*FWHM/2., R_max*FWHM/2.]
	color = ['w', 'orange', 'orange']
	ax.contour(x_gauss,y_gauss,Circle(x_gauss,y_gauss), colors=color, levels = name_contour)

		
	#ax.contour(x_gauss, y_gauss, data_fitted.reshape(N_max_pix_fit,N_max_pix_fit), 4, colors='w')  #A decommenter pour afficher les contour des gaussiennes 2D arbitraires
	#ax.imshow(residu_wavelength, origin='bottom')  #Decommenter et commenter le plot de minimap 3 lignes plus haut pour tracer les residus

	index_wavelength += 1
	


plt.subplots_adjust(bottom=0.1, right=0.8, top=0.9)
fig.colorbar(axes[0].get_images()[0], orientation='horizontal',ax=axes.ravel().tolist())



	



