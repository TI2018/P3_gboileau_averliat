import numpy as np
import scipy as sp
import healpy as hp

from astropy.io import fits
from astropy.table import Table
from astropy import wcs



#Nombre de cluster (max=1653)
N_cluster = 300

N_minimap_random = 100
N_empilement = 100

#Nombre de pixels de la carte
N_pixel_carte = 199  #Doit être impair

#Taille angulaire de la minimap
taille_angulaire_sur_ciel = 1.  #En degres



#============================================================
#Lecture du catalogue
#============================================================
catalogue = Table.read('/home/averliat/P3_gboileau_averliat/catalogue/PSZ2v1.fits')
#catalogue.sort('SNR')  #Tri par rapport signal sur bruit
catalogue.sort('Q_NEURAL')  #Tri par 'qualite' de l'amas
catalogue.reverse()
if 3*N_cluster < 1653:
	catalogue = catalogue[:3*N_cluster]
catalogue.sort('SNR')
catalogue.reverse()
catalogue = catalogue[:N_cluster]



#============================================================
#Lecture des coordonnees glon-glat des clusters
#============================================================
glon_cluster = catalogue['GLON'][0:N_cluster]
glat_cluster = catalogue['GLAT'][0:N_cluster]



#============================================================
#Coefficient de conversion entre les longueurs d'onde
#============================================================
coeff = [244.1,371.74,483.690,287.450,1,1]



#============================================================
#Longueurs d'onde d'observation
#============================================================
wavelength=[100,143,217,353,545,857]

#============================================================
#Lecture des coordonnees glon-glat des clusters
#============================================================

#============================================================
#Debut de la boucle sur les longueurs d'onde
#============================================================
multi_wavelength_minimap = []
for l in range(len(wavelength)):

	#============================================================
	#Lecture des donnees de Planck
	#============================================================
	map_planck = hp.read_map('/home/averliat/Map_planck/HFI_SkyMap_' + str(wavelength[l]) + '_2048_R2.00_full.fits')

	#Tableau regroupant les coordonnees healpix des clusters
	pix_cluster = hp.pixelfunc.ang2pix(2048, glon_cluster, glat_cluster, nest=False, lonlat=True)



	#============================================================
	#Creation des WCS et des minimap
	#============================================================
	centre_carte = N_pixel_carte/2.+0.5  #En pix, pas en coordonnes, donc commun a toute les cartes
	taille_angulaire_un_pixel = taille_angulaire_sur_ciel / N_pixel_carte

	#Definition de 2 tableaux x et y (attention x et y inverses pour glon et glat dans le bon axe)
	y_pix, x_pix = np.indices((N_pixel_carte, N_pixel_carte))

	#Creation d'une carte vierge pour l'empilement
	minimap_stack = np.zeros((N_pixel_carte, N_pixel_carte))

	print("---------------------")
	print("")
	print("")
	print("Wavelength"+str(wavelength[l])+" :")


	# Debut de la creation des WCS
	for n in range(N_cluster):                                            
		print("Avancement : ",round((n+1)/N_cluster*100.,2),"%", end="\r")
		w = wcs.WCS(naxis=2) # Dimension de la carte

		w.wcs.crpix = [centre_carte, centre_carte] #Centre de la projection = centre de la carte
		w.wcs.cdelt = np.array([-taille_angulaire_un_pixel, taille_angulaire_un_pixel])  #Taille angulaire de chaque pixel, un - devant taille en x par convention
		w.wcs.crval = [glon_cluster[n], glat_cluster[n]]  #Coordonnees du centre de la carte
		w.wcs.ctype = ["GLON-TAN", "GLAT-TAN"]  #Projection et systeme de coordonnes
		#Le WCS est cree


		#Remplissage des minimap:
		#Passage de x,y --> glon, glat
		glon_minimap, glat_minimap = w.wcs_pix2world(x_pix, y_pix, 0)

		#Valeur des pixels suivant les valeurs de glon, glat --> construction de la carte
		value_minimap = coeff[l]*map_planck[hp.pixelfunc.ang2pix(2048, glon_minimap, glat_minimap, nest=False, lonlat=True)]

		minimap_stack = minimap_stack + value_minimap

	print("")
	print("---------------------")
	minimap_stack = minimap_stack / N_cluster
	multi_wavelength_minimap.append(minimap_stack)	



#============================================================
#Enregistrement des minimaps en FITS
#============================================================
hdu = fits.PrimaryHDU(multi_wavelength_minimap)

hdu.writeto('/home/averliat/P3_gboileau_averliat/Minimap/Multiwavelength_minimap_Ncluster='+str(N_cluster)+'.fits')




