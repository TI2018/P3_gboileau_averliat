import numpy as np
import scipy as sp

import matplotlib.pyplot as plt
plt.ion()  #Pour que matplotlib affiche le plot direct

#from astropy.io import fits

import healpy as hp


#NSIDE=32
#m = np.arange(hp.nside2npix(NSIDE))
#hp.mollview(m, title="Mollview image RING")


test_map = hp.read_map('/home/averliat/Map_planck/HFI_SkyMap_100_2048_R2.00_full.fits')

hp.mollview(test_map, coord=['G'], title= 'testplot', unit='mK', norm ='hist', min = -0.5, max=0.5, xsize=2000)


#norm = 'hist' est lin mais sature un peu les pix haut et bas pour bonne échelle de visu (a priori pas besoin de min, max)
#coord = ['G']  ou C ou E ou ['X', 'Y'] avec X, Y = G, C ou E


