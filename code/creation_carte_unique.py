import numpy as np
import scipy as sp

import matplotlib.pyplot as plt
plt.ion()  #Pour que matplotlib affiche le plot direct

from astropy.io import fits
from astropy.table import Table

import healpy as hp

from astropy.io import fits
from astropy import wcs


#============================================================
#Lecture du catalogue
#============================================================
catalogue = Table.read('/home/averliat/P3_gboileau_averliat/PSZ2v1.fits')
catalogue.sort('SNR')
catalogue.reverse()



#============================================================
#Lecture des coordonnees glon-glat des clusters
#============================================================

#print(catalogue['GLON','GLAT'][0:2])

glon_cluster_test = catalogue['GLON'][0]
#print(glon_cluster_test)
#glon_cluster_test = glon_cluster_test * np.pi / 180.

glat_cluster_test = catalogue['GLAT'][0]
#print(glat_cluster_test)
#glat_cluster_test = glat_cluster_test * np.pi / 180.




#============================================================
#Lecture des donnees de Plank
#============================================================
#map_planck_1, head_planck_1 = hp.read_map('/home/averliat/Map_planck/HFI_SkyMap_100_2048_R2.00_full.fits', h=True)
map_planck_1 = hp.read_map('/home/averliat/Map_planck/HFI_SkyMap_100_2048_R2.00_full.fits')

pix_cluster = hp.pixelfunc.ang2pix(2048, glon_cluster_test, glat_cluster_test, nest=False, lonlat=True)

print(pix_cluster)

#hp.mollview(test_map, coord=['G'], title= 'testplot', unit='mK', norm ='hist', min = -0.5, max=0.5, xsize=2000)




#============================================================
#Creation du WCS
#============================================================
#A priori ça marche bien pour le stack, c'est comme le WCS mais c'est une boîte noire
#minimap = hp.visufunc.gnomview(map=map_planck_1, fig=None, rot=(glon_cluster_test,glat_cluster_test,0.), coord='G', unit='', xsize=199, ysize=None, reso=0.3, title='Gnomonic view', nest=False, remove_dip=False, remove_mono=False, gal_cut=0, min=None, max=None, flip='astro', format='%.3g', cbar=True, cmap=None, norm=None, hold=False, sub=None, margins=None, notext=False, return_projected_map=False)

N_pixel_carte = 199 #Doit être impair
centre_carte = N_pixel_carte/2.+0.5

taille_angulaire_sur_ciel = 1.
taille_angulaire_un_pixel = taille_angulaire_sur_ciel / N_pixel_carte

# Debut de la creation du WCS
# Dimension de la carte
w = wcs.WCS(naxis=2)

w.wcs.crpix = [centre_carte, centre_carte] #Centre de la projection = centre de la carte
w.wcs.cdelt = np.array([-taille_angulaire_un_pixel, taille_angulaire_un_pixel])  #Taille angulaire de chaque pixel, un - devant taille en x par convention
w.wcs.crval = [glon_cluster_test, glat_cluster_test]  #Coordonnees du centre de la carte
w.wcs.ctype = ["GLON-TAN", "GLAT-TAN"]  #Projection et systeme de coordonnes

#Le WCS est cree


#============================================================
#Creation des mini cartes
#============================================================
#Le code ci-dessous commente fonctionne mais est "lent"
"""
coord_minimap = np.zeros((N_pixel_carte, N_pixel_carte, 2), dtype=float)
value_minimap = np.zeros((N_pixel_carte, N_pixel_carte), dtype=float)

#Passage x,y --> glon, glat
for i in range(N_pixel_carte):
    for j in range(N_pixel_carte):
        coord_minimap[j,i] = w.wcs_pix2world([[i,j]], 1)

#Passage glon, glat --> valeur 
for i in range(N_pixel_carte):
    for j in range(N_pixel_carte):
        value_minimap[i,j] = map_planck_1[hp.pixelfunc.ang2pix(2048, coord_minimap[i,j,0], coord_minimap[i,j,1], nest=False, lonlat=True)]

#Plot de la minimap
Minimap = plt.imshow(value_minimap, cmap=plt.cm.viridis, origin='lower')

plt.show()
"""

#Le code ci-dessous est plus compact et plus rapide:
#Definition de 2 tableaux x et y (attention x et y inverses pour glon et glat dans le bon axe)
y_pix, x_pix = np.indices((N_pixel_carte, N_pixel_carte))

#Passage de x,y --> glon, glat
glon_minimap, glat_minimap = w.wcs_pix2world(x_pix, y_pix, 0)

#Valeur des pixels suivant les valeurs de glon, glat --> construction de la carte
value_minimap = map_planck_1[hp.pixelfunc.ang2pix(2048, glon_minimap, glat_minimap, nest=False, lonlat=True)]

#origin='lower' pour que le plot soit dans le bon sens
plt.imshow(value_minimap, origin = 'lower')

plt.show()


